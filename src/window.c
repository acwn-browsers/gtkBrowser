/*

Filename: window.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#include "window.h"
#include "bookmarks.h"
#include "helper.h"
#include "tabs.h"
#include "preferences.h"


struct _CoronabrowserWindow
{
  GtkApplicationWindow parent;

  GSettings *settings;
  GtkWidget *button_newtab;
  GtkWidget *button_closetab;
  GtkWidget *button_bookmarks;
  GtkWidget *button_preferences;
  GtkWidget *button_about;
  GtkWidget *notebook01;
};

G_DEFINE_TYPE (CoronabrowserWindow, coronabrowser_window, GTK_TYPE_APPLICATION_WINDOW)

static void coronabrowser_window_init (CoronabrowserWindow *win)
{
  gtk_widget_init_template (GTK_WIDGET (win));
  win->settings = g_settings_new ("de.acwn.coronabrowser");
  gb_new_tab (win->notebook01, g_settings_get_string (win->settings, "newtab-page"), FALSE);
  gtk_menu_tool_button_set_menu (GTK_MENU_TOOL_BUTTON (win->button_bookmarks), coronabrowser_bookmarks_new_menu ());
}

static void coronabrowser_window_dispose (GObject *object)
{
  CoronabrowserWindow *win;

  win = CORONABROWSER_WINDOW (object);
  g_clear_object (&win->settings);
  G_OBJECT_CLASS (coronabrowser_window_parent_class)->dispose (object);
}


static void on_button_newtab_clicked (GtkWidget *widget, CoronabrowserWindow *win)
{
  if (g_settings_get_boolean (win->settings, "userdefined-page"))
  {
    gb_new_tab (win->notebook01, g_settings_get_string (win->settings, "newtab-page"), FALSE);
  }
  else
  {
    gb_new_tab (win->notebook01, "", FALSE);
  }
}

static void on_button_closetab_clicked (GtkWidget *widget, CoronabrowserWindow *win)
{
  gb_close_tab (win->notebook01);
}

static void on_button_bookmarks_clicked (GtkWidget *widget)
{
  CoronabrowserWindow *win;
  CoronabrowserBookmarks *dialog;

  win = CORONABROWSER_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (widget)));
  dialog = coronabrowser_bookmarks_new (CORONABROWSER_WINDOW (win));
  gtk_window_present (GTK_WINDOW (dialog));
}

static void on_button_bookmarks_show_menu (GtkMenuToolButton *button, gpointer user_data)
{
}

static void on_button_preferences_clicked (GtkWidget *widget)
{
  CoronabrowserWindow *win;
  CoronabrowserPreferences *dialog;

  win = CORONABROWSER_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (widget)));
  dialog = coronabrowser_preferences_new (CORONABROWSER_WINDOW (win));
  gtk_window_present (GTK_WINDOW (dialog));
}

static void on_button_about_clicked (GtkWidget *widget, CoronabrowserWindow *win)
{
  gb_show_about_dialog (win);
}

static void on_notebook01_page_added (GtkNotebook *notebook, GtkWidget *child, guint page_num, gpointer user_data)
{
}

static void on_notebook01_page_removed (GtkNotebook *notebook, GtkWidget *child, guint page_num, gpointer user_data)
{
}

static void coronabrowser_window_class_init (CoronabrowserWindowClass *class)
{
  G_OBJECT_CLASS (class)->dispose = coronabrowser_window_dispose;
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class), "/de/acwn/coronabrowser/window.ui");

  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserWindow, button_newtab);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserWindow, button_closetab);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserWindow, button_bookmarks);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserWindow, button_preferences);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserWindow, button_about);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserWindow, notebook01);

  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_button_about_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_button_preferences_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_button_bookmarks_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_button_bookmarks_show_menu);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_button_closetab_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_button_newtab_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_notebook01_page_added);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_notebook01_page_removed);
}

CoronabrowserWindow *coronabrowser_window_new (CoronabrowserApp *app)
{
  return g_object_new (CORONABROWSER_WINDOW_TYPE, "application", app, NULL);
}

void coronabrowser_window_open (CoronabrowserWindow *win, gchar *url)
{
  gb_new_tab (win->notebook01, url, FALSE);
}
