/*

Filename: additem.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef ADDITEM_H
#define ADDITEM_H

#include <gtk/gtk.h>

#include "bookmarks.h"

#define CORONABROWSER_ADDITEM_TYPE (coronabrowser_additem_get_type())
G_DECLARE_FINAL_TYPE (CoronabrowserAdditem, coronabrowser_additem, CORONABROWSER, ADDITEM, GtkDialog)

CoronabrowserAdditem *coronabrowser_additem_new (CoronabrowserBookmarks *win);
const gchar *coronabrowser_additem_get_item (CoronabrowserAdditem *win);

#endif
