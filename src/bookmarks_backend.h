/*

Filename: bookmarks_backend.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef __BOOKMARKS_BACKEND__
#define __BOOKMARKS_BACKEND__

#include <gtk/gtk.h>
#include <sqlite3.h>

#include "bookmarks.h"
#include "popup.h"


typedef struct s_backend
{
  sqlite3 *db;
} t_backend;

typedef struct s_bookmark
{
  gint id;
  gchar *title;
  gchar *address;
  gchar *folder;
} t_bookmark;

typedef struct s_folder
{
  gint id;
  gchar *name;
  gint parent_id;
} t_folder;


t_backend *backend_new ();

void backend_free (t_backend *backend);

void backend_add_bookmark (t_backend *backend, const gchar *title, const gchar *uri, const gchar *folder);

gboolean backend_delete_bookmark (t_backend *backend, const t_bookmark *bookmark);

void backend_get_all_bookmarks (t_backend *backend, GtkListStore *win, const gchar *folder);

void backend_add_label (t_backend *backend, const gchar *name);

void backend_delete_label (t_backend *backend, const gchar *name);

void backend_get_child_folders (t_backend *backend, GtkTreeView *treeview, const gchar *folder);

void backend_get_all_labels (t_backend *backend, GtkWidget *widget);

#endif
