/*

Filename: preferences.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#include "preferences.h"

struct _CoronabrowserPreferences
{
  GtkDialog parent;

  GSettings *settings;
  GtkWidget *okbutton;
  GtkWidget *general_emptypage;
  GtkWidget *general_userpage;
  GtkWidget *general_entrypage;
  GtkWidget *general_homepage;
};

G_DEFINE_TYPE (CoronabrowserPreferences, coronabrowser_preferences, GTK_TYPE_DIALOG)


static void coronabrowser_preferences_init (CoronabrowserPreferences *win)
{
  gtk_widget_init_template (GTK_WIDGET (win));
  win->settings = g_settings_new ("de.acwn.coronabrowser");
  g_settings_bind (win->settings, "newtab-page", win->general_entrypage, "text", G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (win->settings, "userdefined-page", win->general_userpage, "active", G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (win->settings, "homepage", win->general_homepage, "text", G_SETTINGS_BIND_DEFAULT);
}

static void coronabrowser_preferences_dispose (GObject *object)
{
  CoronabrowserPreferences *win;

  win = CORONABROWSER_PREFERENCES (object);
  g_clear_object (&win->settings);
  G_OBJECT_CLASS (coronabrowser_preferences_parent_class)->dispose (object);
}

static void on_okbutton_clicked (GtkWidget *widget, CoronabrowserPreferences *win)
{
  gtk_window_close (GTK_WINDOW (win));
}

static void on_general_startpage_toggled (GtkWidget *widget, CoronabrowserPreferences *win)
{
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
  {
    gtk_widget_set_sensitive (GTK_WIDGET (win->general_entrypage), TRUE);
  }
  else
  {
    gtk_widget_set_sensitive (GTK_WIDGET (win->general_entrypage), FALSE);
  }
}

static void coronabrowser_preferences_class_init (CoronabrowserPreferencesClass *class)
{
  G_OBJECT_CLASS (class)->dispose = coronabrowser_preferences_dispose;
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class), "/de/acwn/coronabrowser/preferences.ui");

  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserPreferences, okbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserPreferences, general_emptypage);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserPreferences, general_userpage);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserPreferences, general_entrypage);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserPreferences, general_homepage);

  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_okbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_general_startpage_toggled);
}

CoronabrowserPreferences *coronabrowser_preferences_new (CoronabrowserWindow *win)
{
  return g_object_new (CORONABROWSER_PREFERENCES_TYPE, "transient-for", win, NULL);
}
