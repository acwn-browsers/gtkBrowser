/*

Filename: popup.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#include "popup.h"
#include "helper.h"
#include "tabs.h"
#include "bookmarks_backend.h"

/* primary icon */

typedef struct s_popover_primary
{
  GtkWidget *popup;
  GtkWidget *grid;
  GtkWidget *label;
  GtkWidget *buttonbox;
  GtkWidget *button_ok;
  GtkWidget *button_cancel;
} t_popover_primary;

void gb_create_primary_popup (GtkWidget *relative_to, const GdkRectangle *rect)
{
  t_popover_primary *popover = g_malloc0 (sizeof (t_popover_primary));

  popover->popup = gtk_popover_new (relative_to);
  gtk_container_set_border_width (GTK_CONTAINER (popover->popup), 10);
  gtk_popover_set_pointing_to (GTK_POPOVER (popover->popup), rect);
  popover->grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (popover->grid), 5);
  gtk_container_add (GTK_CONTAINER (popover->popup), popover->grid);
  popover->label = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (popover->label), "<b>Information</b>");
  gtk_grid_attach (GTK_GRID (popover->grid), popover->label, 0, 0, 2, 1);
  /* Signals */

  /* Show all widgets */
  gtk_widget_show_all (popover->grid);
  gtk_popover_popup (GTK_POPOVER (popover->popup));
}

/* end of primary icon */

/* secondary icon */

typedef struct s_popover_secondary
{
  GtkWidget *popup;
  GtkWidget *grid;
  GtkWidget *label;
  GtkWidget *entry_title;
  GtkWidget *entry_uri;
  GtkWidget *combo_folder;
  GtkWidget *buttonbox;
  GtkWidget *button_ok;
  GtkWidget *button_cancel;
} t_popover_secondary;

static void on_button_ok_clicked (GtkButton *button, gpointer user_data)
{
  t_popover_secondary *popover = (t_popover_secondary*) user_data;
  t_backend *be = backend_new ();
  /* Save the bookmark */
  backend_add_bookmark (be, gtk_entry_get_text (GTK_ENTRY (popover->entry_title)), gtk_entry_get_text (GTK_ENTRY (popover->entry_uri)), gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT (popover->combo_folder)));
  gtk_popover_popdown (GTK_POPOVER (popover->popup));
  backend_free (be);
}

static void on_button_cancel_clicked (GtkButton *button, gpointer user_data)
{
  t_popover_secondary *popover = (t_popover_secondary*) user_data;
  
  gtk_popover_popdown (GTK_POPOVER (popover->popup));
}

void gb_create_secondary_popup (GtkWidget *relative_to, const GdkRectangle *rect, const gchar *title, const gchar *uri)
{
  t_popover_secondary *popover = g_malloc0 (sizeof (t_popover_secondary));

  popover->popup = gtk_popover_new (relative_to);
  gtk_container_set_border_width (GTK_CONTAINER (popover->popup), 10);
  gtk_popover_set_pointing_to (GTK_POPOVER (popover->popup), rect);
  popover->grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (popover->grid), 5);
  popover->label = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (popover->label), "<b>Add a bookmark</b>");
  gtk_container_add (GTK_CONTAINER (popover->popup), popover->grid);
  gtk_grid_attach (GTK_GRID (popover->grid), popover->label, 0, 0, 2, 1);
  
  popover->label = gtk_label_new ("Title");
  gtk_grid_attach (GTK_GRID (popover->grid), popover->label, 0, 1, 1, 1);
  popover->entry_title = gtk_entry_new ();
  gtk_grid_attach (GTK_GRID (popover->grid), popover->entry_title, 1, 1, 1, 1);
  gtk_entry_set_text (GTK_ENTRY (popover->entry_title), title);

  popover->label = gtk_label_new ("Address");
  gtk_grid_attach (GTK_GRID (popover->grid), popover->label, 0, 2, 1, 1);
  popover->entry_uri = gtk_entry_new ();
  gtk_grid_attach (GTK_GRID (popover->grid), popover->entry_uri, 1, 2, 1, 1);
  gtk_entry_set_text (GTK_ENTRY (popover->entry_uri), uri);

  popover->label = gtk_label_new ("Label");
  gtk_grid_attach (GTK_GRID (popover->grid), popover->label, 0, 3, 1, 1);
  popover->combo_folder = gtk_combo_box_text_new ();
  t_backend *be = backend_new ();
  backend_get_all_labels (be, popover->combo_folder);
  backend_free (be);
  gtk_combo_box_set_active (GTK_COMBO_BOX (popover->combo_folder), 0);
  gtk_grid_attach (GTK_GRID (popover->grid), popover->combo_folder, 1, 3, 1, 1);
  
  popover->buttonbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 5);
  gtk_box_set_homogeneous (GTK_BOX (popover->buttonbox), TRUE);
  popover->button_cancel = gtk_button_new_with_label ("Cancel");
  popover->button_ok = gtk_button_new_with_label ("OK");
  gtk_box_pack_start (GTK_BOX (popover->buttonbox), popover->button_cancel, TRUE, TRUE, 5);
  gtk_box_pack_start (GTK_BOX (popover->buttonbox), popover->button_ok, TRUE, TRUE, 5);
  gtk_grid_attach (GTK_GRID (popover->grid), popover->buttonbox, 0, 4, 2, 1);

  /* Signals */  
  g_signal_connect (popover->button_ok, "clicked", G_CALLBACK (on_button_ok_clicked), popover);
  g_signal_connect (popover->button_cancel, "clicked", G_CALLBACK (on_button_cancel_clicked), popover);

  /* Show all widgets */
  gtk_widget_show_all (popover->grid);
  gtk_popover_popup (GTK_POPOVER (popover->popup));
}

/* end of secondary icon */
