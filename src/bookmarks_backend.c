/*

Filename: bookmarks_backend.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/


#include <stdlib.h>

#include "bookmarks_backend.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


gboolean backend_execute (t_backend *backend, const gchar *query)
{
  gint result = 0;
  gchar *emsg = NULL;
  
  result = sqlite3_exec (backend->db, query, NULL, NULL, &emsg);
  if (G_UNLIKELY (result != SQLITE_OK))
  {
    g_print ("Error: sqlite3 query '%s' failed! Error(%d): %s\n", query, result, emsg);
    sqlite3_free (emsg);
    return FALSE;
  }
  else
  {
    sqlite3_free (emsg);
    return TRUE;
  }
}

const gchar *backend_get_folder_id (t_backend *backend, const gchar *folder)
{
  gint folder_id = 0;
	gint result = 0;
	sqlite3_stmt *stmt;
	gchar *query = g_strconcat ("SELECT id FROM labels WHERE name = '", folder, "' LIMIT 1;", NULL);
	
	result = sqlite3_prepare_v2 (backend->db, query, -1, &stmt, NULL);
	if (result != SQLITE_OK)
	{
	  return 0;
	}
	while (sqlite3_step (stmt) == SQLITE_ROW)
	{
		folder_id = sqlite3_column_int (stmt, 0);
	}
	if (sqlite3_finalize (stmt) != SQLITE_OK)
	{
	  return 0;
	}
	g_free (query);
	const gchar *folder_id_string = g_strdup_printf("%i", folder_id);
	return folder_id_string;
}

t_backend *backend_new ()
{
  gint result;
  t_backend *be = g_malloc0 (sizeof (t_backend));
  const gchar *homedir = g_get_home_dir ();
  const gchar *dbfile = g_build_path ("/", homedir, ".config", PACKAGE_NAME, "bookmarks.db", NULL);

  result = sqlite3_open (dbfile, &be->db);
  if (result != SQLITE_OK)
  {
    sqlite3_close (be->db);
  }
  else
  {
    const gchar *query01 = "CREATE TABLE IF NOT EXISTS bookmarks (id INTEGER PRIMARY KEY ASC NOT NULL, title TEXT UNIQUE ON CONFLICT IGNORE, address TEXT);";
    const gchar *query02 = "CREATE TABLE IF NOT EXISTS labels (id INTEGER PRIMARY KEY ASC NOT NULL, name TEXT UNIQUE ON CONFLICT IGNORE);";
    const gchar *query03 = "INSERT OR IGNORE INTO labels (id, name) VALUES (0, 'Labels');";
    const gchar *query04 = "CREATE TABLE IF NOT EXISTS bookmarkxlabel (bookmark_id integer, label_id integer);";

    backend_execute (be, query01);
    backend_execute (be, query02);
    backend_execute (be, query03);
    backend_execute (be, query04);
  }
  return be;
}

void backend_free (t_backend *backend)
{
  sqlite3_close (backend->db);
  g_free (backend);
}

void backend_add_bookmark (t_backend *backend, const gchar *title, const gchar *uri, const gchar *folder)
{
  gchar *query01 = g_strconcat ("INSERT INTO bookmarks (title,address) VALUES ('", title, "','", uri, "');", NULL);
  gchar *query02 = g_strconcat ("INSERT INTO labels (name) VALUES ('", folder,"');",NULL);
  backend_execute (backend, query01);
  backend_execute (backend, query02);
  const gchar *folder_id = backend_get_folder_id (backend, folder);
  g_free (query01);
  g_free (query02);
}

gboolean backend_delete_bookmark (t_backend *backend, const t_bookmark *bookmark)
{
  gboolean retval;
  gchar *query = g_strconcat ("DELETE FROM bookmarks WHERE title = '", bookmark->title, "' AND address='", bookmark->address,"';", NULL);

  retval = backend_execute (backend, query);
  g_free (query);
  return retval;
}

static gint backend_exec_callback_bookmarks (void *win, gint row, gchar **column_text, gchar **column_name)
{
  GtkTreeIter iter;
  t_bookmark *row_data = g_malloc0 (sizeof (t_bookmark));
  for (gint i = 0; i < row; ++i)
  {
    if (g_strcmp0 (column_name[i], "id") == 0)
    {
      row_data->id = atoi (column_text [i]);
    }
    if (g_strcmp0 (column_name[i], "title") == 0)
    {
      row_data->title = column_text [i];
    }
    if (g_strcmp0 (column_name[i], "address") == 0)
    {
      row_data->address = column_text [i];
    }
    if (g_strcmp0 (column_name[i], "folder") == 0)
    {
      row_data->folder = column_text [i];
    }
  }
  gtk_list_store_append (win, &iter);
  gtk_list_store_set (win, &iter, 0, row_data->title, 1, row_data->address,  -1);
  g_free (row_data);
  return 0;
}

void backend_get_all_bookmarks (t_backend *backend, GtkListStore *win, const gchar *folder)
{
  gint result = 0;
//  gchar *query = g_strconcat ("SELECT title, address FROM bookmarks LEFT JOIN labels ON bookmarks.folder_id=labels.id WHERE labels.name='", folder, "';", NULL);
  gchar *query = g_strconcat ("SELECT title, address FROM bookmarks JOIN bookmarkxlabel ON bookmarks.id=bookmarkxlabel.bookmark_id JOIN labels ON bookmarkxlabel.label_id=labels.id WHERE labels.name='", folder, "';", NULL);
  gchar *emsg = NULL;

  result = sqlite3_exec (backend->db, query, backend_exec_callback_bookmarks, win, &emsg);
  if (result != SQLITE_OK)
  {
    g_print ("Error: sqlite3 query '%s' failed! Error(%d): %s\n", query, result, emsg);
  }
  sqlite3_free (emsg);
  g_free (query);
}

void backend_add_label (t_backend *backend, const gchar *name)
{
  gchar *query = g_strconcat ("INSERT INTO labels (name) VALUES ('", name, "')", NULL);
  if (!backend_execute (backend, query))
  {
    g_print ("Failed to save a new label.");
  }
  g_free (query);
}

void backend_delete_label (t_backend *backend, const gchar *name)
{
  gchar *query = g_strconcat ("DELETE FROM labels WHERE name = '", name, "')", NULL);
  
  g_free (query);
}

static gint backend_exec_callback_folders (void *treeview, gint row, gchar **column_text, gchar **column_name)
{
  GtkTreeIter child;
  GtkTreeIter parent;
  GtkTreeModel *model;
  t_folder *row_data = g_malloc0 (sizeof (t_folder));
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection (treeview);
  gtk_tree_selection_get_selected (selection, &model, &parent);
  
  for (gint i = 0; i < row; ++i)
  {
    if (g_strcmp0 (column_name[i], "id") == 0)
    {
      row_data->id = atoi (column_text [i]);
    }
    if (g_strcmp0 (column_name[i], "name") == 0)
    {
      row_data->name = column_text [i];
    }
    if (g_strcmp0 (column_name[i], "parent_id") == 0)
    {
      row_data->parent_id = atoi (column_text [i]);
    }
  }
  gchar *name_temp = row_data->name;
  gtk_tree_store_append (GTK_TREE_STORE (model), &child, &parent);
  gtk_tree_store_set (GTK_TREE_STORE (model), &child, 0, row_data->name, -1);
  g_free (row_data);
  return 0;
}

void backend_get_child_folders (t_backend *backend, GtkTreeView *treeview, const gchar *folder)
{
  gint result = 0;
  gchar *emsg = NULL;
  const gchar *query = "SELECT name FROM labels WHERE name != 'Labels' ORDER BY name ASC;";

  result = sqlite3_exec (backend->db, query, backend_exec_callback_folders, treeview, &emsg);
  if (result != SQLITE_OK)
  {
    g_print ("Error: sqlite3 query '%s' failed! Error(%d): %s\n", query, result, emsg);
  }
  sqlite3_free (emsg);
}

gint backend_exec_callback_all_folders (void *widget, gint row, gchar **column_text, gchar **column_name)
{
  for (gint i = 0; i < row; ++i)
  {
    if (g_strcmp0 (column_name[i], "name") == 0)
    {
      gtk_combo_box_text_append_text (widget, column_text [i]);
    }
  }
  return 0;
}

void backend_get_all_labels (t_backend *backend, GtkWidget *widget)
{
  gchar *emsg = NULL; 
  gint result;
  const gchar *query = "SELECT name FROM labels WHERE name != 'Labels' ORDER BY name ASC;";
  result = sqlite3_exec (backend->db, query, backend_exec_callback_all_folders, widget, &emsg);
  if (result != SQLITE_OK)
  {
    g_print ("Error: sqlite3 query '%s' failed! Error(%d): %s\n", query, result, emsg);
  }
  sqlite3_free (emsg);
}
