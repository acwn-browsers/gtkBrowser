/*

Filename: main.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#include <gtk/gtk.h>
#include "application.h"

gint main (gint argc, gchar *argv[])
{
  return g_application_run (G_APPLICATION (coronabrowser_new ()), argc, argv);
}
