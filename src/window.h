/*

Filename: window.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef WINDOW_H
#define WINDOW_H

#include <gtk/gtk.h>
#include "application.h"

#define CORONABROWSER_WINDOW_TYPE (coronabrowser_window_get_type())
G_DECLARE_FINAL_TYPE (CoronabrowserWindow, coronabrowser_window, CORONABROWSER, WINDOW, GtkApplicationWindow)

CoronabrowserWindow *coronabrowser_window_new (CoronabrowserApp *app);
void coronabrowser_window_open (CoronabrowserWindow *win, gchar *url);

#endif
