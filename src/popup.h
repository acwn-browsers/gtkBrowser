/*

Filename: popup.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef __POPUP_H__
#define __POPUP_H__

#include <gtk/gtk.h>

void gb_create_primary_popup (GtkWidget *relative_to, const GdkRectangle *rect);

void gb_create_secondary_popup (GtkWidget *relative_to, const GdkRectangle *rect, const gchar *title, const gchar *uri);

#endif
